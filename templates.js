module.exports = {
  html: function (result) {
    if (result.error) {
      return result.error.message
    }

    if (!Array.isArray(result.items) || result.items.length === 0) {
      return ''
    }

    var eventsByDate = new Map()
    result.items.forEach((event) => {
      var key = event.start.date || event.start.dateTime.substr(0, 10);
      if (!eventsByDate.has(key)) {
        eventsByDate.set(key, [])
      }
      eventsByDate.get(key).push(event)
    })

    var html = ''
    for (var date of eventsByDate.keys()) {
      var events = eventsByDate.get(date)
      html += `<p>Am ${date.split('-').reverse().join('.')} ist `
      var links = events.map((event) => {
        return `<a href="https://kalender.fonflatter.de/">${event.summary}</a>`
      })

      if (links.length == 1) {
        html += links[0]
      } else  {
        var lastLink = links.pop()
        html += links.join(', ')
        html += ' und ' + lastLink
      }

      html += '.</p>\n'
    }

    return html
  }
}
