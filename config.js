var rc = require('rc')

module.exports = rc('kalender.fonflatter.de', {
  googleapis: rc('googleapis', {
    auth: {
      apiKey: undefined
    },
    calendar: {
      v3: {
        calendarId: undefined
      }
    }
  })
})
