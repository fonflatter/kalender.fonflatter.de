var config = require('./config')
var express = require('express')
var expressCalendar = require('express-calendar')

var options = {
    calendarId: config.googleapis.calendar.v3.calendarId,
    parameters: {
        key: config.googleapis.auth.apiKey,
        fields: ['items/summary', 'items/id', 'items/start'].join(','),
        maxResults: 1000
    }
}

options.templates = require('./templates')

var app = express()
expressCalendar(app, options)

module.exports = app
